<?php

/**
 * This is the model class for table "dso_master_price_item".
 *
 * The followings are the available columns in table 'dso_master_price_item':
 * @property integer $id
 * @property string $part_code
 * @property integer $price
 * @property string $top
 * @property string $disc_code
 * @property integer $price_after_disc
 * @property string $period_start
 * @property string $period_end
 * @property string $created_by
 * @property string $created_date
 * @property string $modified_by
 * @property string $modified_date
 */
class DsoMasterPriceItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dso_master_price_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('price, price_after_disc', 'numerical', 'integerOnly'=>true),
			array('part_code', 'length', 'max'=>20),
			array('top, disc_code,disc_rate', 'length', 'max'=>2),
			array('created_by, modified_by', 'length', 'max'=>30),
			array('period_start, period_end, created_date, modified_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, part_code, price, top, disc_code, price_after_disc, period_start, period_end, created_by, created_date, modified_by, modified_date,disc_rate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'part_code' => 'Part Code',
			'price' => 'Price',
			'top' => 'Top',
			'disc_code' => 'Disc Code',
			'price_after_disc' => 'Price After Disc',
			'period_start' => 'Period Start',
			'period_end' => 'Period End',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'modified_by' => 'Modified By',
			'modified_date' => 'Modified Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('part_code',$this->part_code,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('top',$this->top,true);
		$criteria->compare('disc_code',$this->disc_code,true);
		$criteria->compare('price_after_disc',$this->price_after_disc);
		$criteria->compare('period_start',$this->period_start,true);
		$criteria->compare('period_end',$this->period_end,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modified_by',$this->modified_by,true);
		$criteria->compare('modified_date',$this->modified_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchbydealer()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('part_code',$this->part_code,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('top',$this->top,true);
		$criteria->compare('disc_code',$this->disc_code,true);
		$criteria->compare('price_after_disc',$this->price_after_disc);
		$criteria->compare('period_start',$this->period_start,true);
		$criteria->compare('period_end',$this->period_end,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modified_by',$this->modified_by,true);
		$criteria->compare('modified_date',$this->modified_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DsoMasterPriceItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
