<?php

/**
 * This is the model class for table "master_part".
 *
 * The followings are the available columns in table 'master_part':
 * @property integer $part_id
 * @property string $part_name
 * @property string $part_no
 * @property integer $pricelist
 * @property integer $km1th
 * @property integer $id_dealer
 * @property string $category
 */
class MasterPart extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'master_part';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('part_name, part_no, pricelist, km1th, category', 'required'),
			array('pricelist, km1th', 'numerical', 'integerOnly'=>true),
			array('part_name', 'length', 'max'=>100),
			array('part_no', 'length', 'max'=>20),
			array('category', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('part_id, part_name, part_no, pricelist, km1th, category', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'part_id' => 'Part',
			'part_name' => 'Part Name',
			'part_no' => 'Part No',
			'pricelist' => 'Pricelist',
			'km1th' => 'Km1th',
			'id_dealer' => 'Id Dealer',
			'category' => 'Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('part_id',$this->part_id);
		$criteria->compare('part_name',$this->part_name,true);
		$criteria->compare('part_no',$this->part_no,true);
		$criteria->compare('pricelist',$this->pricelist);
		$criteria->compare('km1th',$this->km1th);
		$criteria->compare('id_dealer',$this->id_dealer);
		$criteria->compare('category',$this->category,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterPart the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
