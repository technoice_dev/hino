<?php
/* @var $this PartbytypeController */
/* @var $model Partbytype */

$this->breadcrumbs=array(
	'Partbytypes'=>array('index'),
	$model->typemtc_id,
);

$this->menu=array(
	array('label'=>'List Partbytype', 'url'=>array('index')),
	array('label'=>'Create Partbytype', 'url'=>array('create')),
	array('label'=>'Update Partbytype', 'url'=>array('update', 'id'=>$model->typemtc_id)),
	array('label'=>'Delete Partbytype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->typemtc_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Partbytype', 'url'=>array('admin')),
);
?>

<h1>View Partbytype #<?php echo $model->typemtc_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'typemtc_id',
		'unit_id',
		'part_id',
		'qty',
		'kminterval',
		'probabilitas',
		'frek_ganti',
		'mileage',
	),
)); ?>
