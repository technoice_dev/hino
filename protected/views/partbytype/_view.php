<?php
/* @var $this PartbytypeController */
/* @var $data Partbytype */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('typemtc_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->typemtc_id), array('view', 'id'=>$data->typemtc_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_id')); ?>:</b>
	<?php echo CHtml::encode($data->unit_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('part_id')); ?>:</b>
	<?php echo CHtml::encode($data->part_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qty')); ?>:</b>
	<?php echo CHtml::encode($data->qty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kminterval')); ?>:</b>
	<?php echo CHtml::encode($data->kminterval); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('probabilitas')); ?>:</b>
	<?php echo CHtml::encode($data->probabilitas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('frek_ganti')); ?>:</b>
	<?php echo CHtml::encode($data->frek_ganti); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mileage')); ?>:</b>
	<?php echo CHtml::encode($data->mileage); ?>
	<br />

	*/ ?>

</div>