<?php

@ob_start();
@session_start();


// Untuk setting session bahasa 

$baseUrl = Yii::app()->request->baseUrl;
$lang = Yii::app()->session['sess_lang'];

if (!empty($lang)) {

    Yii::app()->language = $lang;
} else {

    Yii::app()->language = 'en';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- put in head tag -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl . '/image/hino_logo.png'; ?>" type="image/x-icon" />

    <title>Hino Global System</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/css/plugins/timeline.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/aceadmin/assets/css/jquery-ui.min.css" rel="stylesheet">
    
    
    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/css/sb-admin-2.css" rel="stylesheet">


    <!-- Morris Charts CSS
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/css/plugins/morris.css" rel="stylesheet">
    -->


    <!-- Custom Fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/font-opensans/open-sans.css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery Version 1.11.0 
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/jquery-1.11.0.js"></script>
    -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/validator.js"></script>
    <!-- Morris Charts JavaScript 
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/plugins/morris/morris.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/plugins/morris/morris-data.js"></script>
    -->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/sb-admin-2.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/aceadmin/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/aceadmin/assets/js/moment.min.js"></script>
    
    


</head>

<?php if (!Yii::app()->user->isGuest) { ?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <!-- Application Header -->
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="#" style='margin-bottom:10px;'>
                    <img src="<?php echo Yii::app()->request->baseUrl . '/image/hino_logo.png'; ?>" title='logo'  style="width:35px;height:35px;"/>
                    <b>Hino Intagrated System</b>
                    <?php //echo Yii::app()->name; 
                    ?>
                </a>
            </div>
            <?php
            //if(Yii::app()->controller->id == 'site'  && Yii::app()->controller->action->id == 'index')

            ?>

            <!-- Application Header -->

            <!-- Right Header Navigation -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->

                <?php if (!Yii::app()->user->isGuest) { ?>
                    <li class="dropdown">
                        <?php //echo CHtml::link('<i class="fa fa-flag"></i>&nbsp;Indonesia', array('site/languages', 'typelangs' => 'id')); ?>
                    </li>
                    <li>
                        <?php //echo CHtml::link('<i class="fa fa-flag"></i>&nbsp;English', array('site/languages', 'typelangs' => 'en')); ?>
                        <!-- /.dropdown-user -->
                    </li>

                    <li class="dropdown">
                        |<b style='color:blue;'>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo Yii::t('menu', 'Hallo'); ?>,&nbsp;&nbsp;</b><i class="fa fa-user"></i> <?php echo Yii::app()->user->username; ?>&nbsp;&nbsp;&nbsp;&nbsp;|
                    </li>
                <?php } ?>
                <li class="dropdown">
                    <?php

                    $log_state = array('label' => 'Login', 'url' => 'index.php?r=site/login');

                    if (Yii::app()->user->isGuest) { ?>
                        <a href="<?php echo Yii::app()->baseUrl; ?>/site/login"><i class="fa fa-sign-out fa-fw"></i> <?php echo Yii::t('menu', 'Logout'); ?></a>
                    <?php } else { ?>
                        <a href="<?php echo Yii::app()->baseUrl; ?>/site/logout"><i class="fa fa-sign-out fa-fw"></i> <?php echo Yii::t('menu', 'Logout'); ?></a>
                    <?php
                    }
                    ?>
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- Right Header Navigation -->
            <br>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--<li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div-->
                        <!-- /input-group -->
                        <?php if(Yii::app()->user->level !== 'super'){?>
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/penawaran"><i class="fa fa-cog fa-fw"></i> <?php echo Yii::t('menu', 'Penawaran'); ?></a>
                        </li>
                        
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/manpower"><i class="fa fa-user fa-fw"></i> <?php echo Yii::t('menu', 'Manpower'); ?></a>
                        </li>
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/investment"><i class="fa fa-usd fa-fw"></i> <?php echo Yii::t('menu', 'Investment'); ?></a>
                        </li>
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/partbytype"><i class="fa fa-gear fa-fw"></i> <?php echo Yii::t('menu', 'Part By Type'); ?></a>
                        </li>
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/customer"><i class="fa fa-users fa-fw"></i> <?php echo Yii::t('menu', 'Customer'); ?></a>
                        </li>
                        <?php } if(Yii::app()->user->level !== 'sales' ){ 
                            if(Yii::app()->user->level == 'super'){
                        ?>
                        
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/masterPart"><i class="fa fa-cog fa-fw"></i> <?php echo Yii::t('menu', 'Master Part'); ?></a>
                        </li>
                            <?php } 
                            if(Yii::app()->user->level == 'super' ){ ?>
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/masterDealer"><i class="fa fa-truck fa-fw"></i> <?php echo Yii::t('menu', 'Master Dealer'); ?></a>
                        </li>
                        <li>    
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/unitmaster"><i class="fa fa-car fa-fw"></i> <?php echo Yii::t('menu', 'Unit Master'); ?></a>
                        </li>
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/user"><i class="fa fa-user fa-fw"></i> <?php echo Yii::t('menu', 'User'); ?></a>
                        </li>
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/default/importFromExcel"><i class="fa fa-user fa-fw"></i> <?php echo Yii::t('menu', 'Import Part Data From Excel'); ?></a>
                        </li>
                        <?php } 
                            if(Yii::app()->user->level == 'dealer'){
                        ?>
                        
                            <?php } if(Yii::app()->user->level == 'super' ){ ?>
                        
                        <?php } 
                            if(Yii::app()->user->level == 'dealer'){
                        ?>
                        
                        
                            <?php }
                        
                            }
                            if(Yii::app()->user->level == 'sales' || Yii::app()->user->level == 'dealer'){
                            ?>
                        
                        <li>
                            <a class="" href="<?php echo Yii::app()->baseUrl; ?>/Cpk/default/cpkCalculate"><i class="fa fa-book fa-fw"></i> <?php echo Yii::t('menu', 'CPK'); ?></a>
                        </li>
                            <?php } ?>

                        
                
            </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">

            <!-- Dynamic Content -->

            </br></br>

            <!-- Breadcrumbs -->
            <?php if (isset($this->breadcrumbs)) : ?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'homeLink' => false,
                    'tagName' => 'ul',
                    'separator' => '',
                    'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                    'inactiveLinkTemplate' => '<li><span>{label}</span></li>',
                    'htmlOptions' => array('class' => 'breadcrumb'),
                )); ?>

            <?php endif ?>
            <!-- Breadcrumbs -->
            
            <div class="col-6 col-s-9">
            <!-- Page Content -->
                <?php echo $content; ?>
            <!-- Page Content -->
            </div>
            
            <div class="clear"></div>

            <!-- footer -->
            <div id="footer">
                <?php echo Yii::app()->params['appFooter']; ?>
                <?php //echo Yii::powered(); 
                ?>
            </div>
            <!-- footer -->

            <!-- Dynamic Content -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/pirates/js/plugins/metisMenu/metisMenu.min.js"></script>



</body>
<?php } else { 
    echo $content; 
} ?>
    
<script>
    console.log(8);
    $('input[type="number"]').on("change keyup keypres keydown",function(){
        
        if(parseInt($(this).val()) < 0){
            $(this).val(0);
        }

    });
    $('.numeric-field').keyup(function(){
        var default_val = $(this).val();
        default_val = default_val.replace(new RegExp(/[^\d]/,'ig') , "");
        var number_formatted = format_num(default_val);
        this.value = number_formatted;
    });
    $('.km-field').on('keyup keydown change',function(){
        var a = $(this).val();
        var removeSymbol = a.replace(/[-!$%^&*()_+|~=`{}\[\]:";'<>?,\/]/g,'');
        var removeLetter = removeSymbol.replace(/[a-zA-Z]/g, "");
        $(this).val(removeLetter);
    });
    function format_num(number) {
        number = number.replace(/,/g,'');
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    $("#cpk-cost-value").submit(function(e){
        var costCb = 0;
        $(".cost-cb").each(function(){
            if(this.checked){
                costCb += 1;
            }
        });        
    });
    
</script>

</html>