<?php
/* @var $this InvPowerController */
/* @var $model InvPower */

$this->breadcrumbs=array(
	'Inv Powers'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List InvPower', 'url'=>array('index')),
	array('label'=>'Create InvPower', 'url'=>array('create')),
	array('label'=>'Update InvPower', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete InvPower', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvPower', 'url'=>array('admin')),
);
?>

<h1>View InvPower #<?php echo $model->inv_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'inv_id',
		'inv_item',
		'inv_price',
		'inv_qty',
	),
)); ?>
