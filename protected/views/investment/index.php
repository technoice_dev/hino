<?php
/* @var $this InvestmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Investments',
);

$this->menu=array(
	array('label'=>'Create Investment', 'url'=>array('create')),
	array('label'=>'Manage Investment', 'url'=>array('admin')),
);
?>

<h1>Investments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
