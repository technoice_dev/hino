<?php
/* @var $this InvestmentController */
/* @var $model Investment */

$this->breadcrumbs=array(
	'Investments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Investment', 'url'=>array('index')),
	array('label'=>'Manage Investment', 'url'=>array('admin')),
);
?>

<h1>Create Investment</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>