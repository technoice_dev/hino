<?php
/* @var $this InvSafetyController */
/* @var $model InvSafety */

$this->breadcrumbs=array(
	'Inv Safeties'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvSafety', 'url'=>array('index')),
	array('label'=>'Create InvSafety', 'url'=>array('create')),
	array('label'=>'View InvSafety', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage InvSafety', 'url'=>array('admin')),
);
?>

<h1>Update InvSafety <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>