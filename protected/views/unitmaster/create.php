<?php
/* @var $this UnitmasterController */
/* @var $model Unitmaster */

$this->breadcrumbs=array(
	'Unitmasters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Unitmaster', 'url'=>array('index')),
	array('label'=>'Manage Unitmaster', 'url'=>array('admin')),
);
?>

<h1>Create Unitmaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>