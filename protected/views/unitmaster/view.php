<?php
/* @var $this UnitmasterController */
/* @var $model Unitmaster */

$this->breadcrumbs=array(
	'Unitmasters'=>array('index'),
	$model->unit_id,
);

$this->menu=array(
	array('label'=>'List Unitmaster', 'url'=>array('index')),
	array('label'=>'Create Unitmaster', 'url'=>array('create')),
	array('label'=>'Update Unitmaster', 'url'=>array('update', 'id'=>$model->unit_id)),
	array('label'=>'Delete Unitmaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->unit_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Unitmaster', 'url'=>array('admin')),
);
?>

<h1>View Unitmaster #<?php echo $model->unit_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'unit_id',
		'unit_type',
		'unit_name',
	),
)); ?>
