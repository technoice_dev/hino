<?php
/* @var $this InvMeasuringController */
/* @var $model InvMeasuring */

$this->breadcrumbs=array(
	'Inv Measurings'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvMeasuring', 'url'=>array('index')),
	array('label'=>'Create InvMeasuring', 'url'=>array('create')),
	array('label'=>'View InvMeasuring', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage InvMeasuring', 'url'=>array('admin')),
);
?>

<h1>Update InvMeasuring <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>