<?php
/* @var $this InvMeasuringController */
/* @var $model InvMeasuring */

$this->breadcrumbs=array(
	'Inv Measurings'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List InvMeasuring', 'url'=>array('index')),
	array('label'=>'Create InvMeasuring', 'url'=>array('create')),
	array('label'=>'Update InvMeasuring', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete InvMeasuring', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvMeasuring', 'url'=>array('admin')),
);
?>

<h1>View InvMeasuring #<?php echo $model->inv_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'inv_id',
		'inv_item',
		'inv_price',
		'inv_qty',
	),
)); ?>
