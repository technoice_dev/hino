<?php
/* @var $this InvMeasuringController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv Measurings',
);

$this->menu=array(
	array('label'=>'Create InvMeasuring', 'url'=>array('create')),
	array('label'=>'Manage InvMeasuring', 'url'=>array('admin')),
);
?>

<h1>Inv Measurings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
