<?php
/* @var $this InvSstRangerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv Sst Rangers',
);

$this->menu=array(
	array('label'=>'Create InvSstRanger', 'url'=>array('create')),
	array('label'=>'Manage InvSstRanger', 'url'=>array('admin')),
);
?>

<h1>Inv Sst Rangers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
