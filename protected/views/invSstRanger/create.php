<?php
/* @var $this InvSstRangerController */
/* @var $model InvSstRanger */

$this->breadcrumbs=array(
	'Inv Sst Rangers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InvSstRanger', 'url'=>array('index')),
	array('label'=>'Manage InvSstRanger', 'url'=>array('admin')),
);
?>

<h1>Create InvSstRanger</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>