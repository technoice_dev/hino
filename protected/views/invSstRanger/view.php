<?php
/* @var $this InvSstRangerController */
/* @var $model InvSstRanger */

$this->breadcrumbs=array(
	'Inv Sst Rangers'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List InvSstRanger', 'url'=>array('index')),
	array('label'=>'Create InvSstRanger', 'url'=>array('create')),
	array('label'=>'Update InvSstRanger', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete InvSstRanger', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvSstRanger', 'url'=>array('admin')),
);
?>

<h1>View InvSstRanger #<?php echo $model->inv_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'inv_id',
		'inv_item',
		'inv_price',
		'inv_qty',
	),
)); ?>
