<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Mechanic', 'url'=>array('index')),
	array('label'=>'Manage Mechanic', 'url'=>array('admin')),
);
?>

<h1>Create Mechanic</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>