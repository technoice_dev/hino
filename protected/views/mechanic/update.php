<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->nik=>array('view','id'=>$model->nik),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mechanic', 'url'=>array('index')),
	array('label'=>'Create Mechanic', 'url'=>array('create')),
	array('label'=>'View Mechanic', 'url'=>array('view', 'id'=>$model->nik)),
	array('label'=>'Manage Mechanic', 'url'=>array('admin')),
);
?>

<h1>Update Mechanic <?php echo $model->nik; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>