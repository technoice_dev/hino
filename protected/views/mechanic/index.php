<?php
/* @var $this MechanicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mechanics',
);

$this->menu=array(
	array('label'=>'Create Mechanic', 'url'=>array('create')),
	array('label'=>'Manage Mechanic', 'url'=>array('admin')),
);
?>

<h1>Mechanics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
