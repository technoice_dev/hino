<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'master-part-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'part_name'); ?>
		<?php echo $form->textField($model,'part_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'part_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'part_no'); ?>
		<?php echo $form->textField($model,'part_no',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'part_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pricelist'); ?>
		<?php echo $form->textField($model,'pricelist'); ?>
		<?php echo $form->error($model,'pricelist'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'km1th'); ?>
		<?php echo $form->textField($model,'km1th'); ?>
		<?php echo $form->error($model,'km1th'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->