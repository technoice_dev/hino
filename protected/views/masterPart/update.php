<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Master Parts'=>array('index'),
	$model->part_id=>array('view','id'=>$model->part_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MasterPart', 'url'=>array('index')),
	array('label'=>'Create MasterPart', 'url'=>array('create')),
	array('label'=>'View MasterPart', 'url'=>array('view', 'id'=>$model->part_id)),
	array('label'=>'Manage MasterPart', 'url'=>array('admin')),
);
?>

<h1>Update MasterPart <?php echo $model->part_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'optionDealer'=>$optionDealer)); ?>