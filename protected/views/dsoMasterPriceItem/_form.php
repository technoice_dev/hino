<?php
/* @var $this DsoMasterPriceItemController */
/* @var $model DsoMasterPriceItem */
/* @var $form CActiveForm */
?>

<div class="form">
<!--<script src="javascript/nn.echscript.js"></script>-->
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dso-master-price-item-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
<?php echo $form->errorSummary($model); ?>
	<table>
			<tr>
				
									<td >
                                        <?php echo $form->labelEx($model, 'top'); ?>
									</td>
								 
									<td>
										<?php echo $form->dropDownList($model, 'top', array(
												'C1' => 'C1',
												'C2' => 'C2',
												'C3' => 'C3',
												),
												array('class'=>'form-control input-sm', 
												'style'=>'width:200px;'),array('id'=>'top')
												
												); ?>
									</td>
			</tr>
			<tr>
				<td>
					<?php echo $form->labelEx($model,'part_code'); ?>
				</td>
				<td>
					<?php echo $form->textField($model,'part_code',array('size'=>50,'maxlength'=>50,'class' => 'form-control input-sm',)); ?>
					<?php echo $form->error($model,'part_code'); ?>	
				</td>
			</tr>
			<tr>
				<td>
					<?php echo $form->labelEx($model,'price'); ?>
				</td>
				<td>
					<?php echo $form->textField($model,'price',array('numerical'=>true,'size'=>60,'maxlength'=>255,'class' => 'form-control input-sm','id'=>'price','onkeypress'=>'isNumberKey(event)','onkeyup'=>'formatCurrency("price")')); ?>
					<?php echo $form->error($model,'price'); ?>
				</td>
			</tr>
			
			<tr>
				<td>
					<?php echo $form->labelEx($model,'disc_rate'); ?>
				</td>
				<td>
					<?php echo $form->textField($model,'disc_rate',array('size'=>60,'maxlength'=>255,'class' => 'form-control input-sm','id'=>'disc_rate')); ?>
					<?php echo $form->error($model,'disc_rate'); ?>
				</td>
			</tr>
			
			<tr>
				<td>
					<?php echo $form->labelEx($model,'price_after_disc'); ?>
				</td>
				<td>
					<?php echo $form->textField($model,'price_after_disc',array('size'=>60,'maxlength'=>255,'class' => 'form-control input-sm','id'=>'price_after_disc','readonly'=>true)); ?>
					<?php echo $form->error($model,'price_after_disc'); ?>
				</td>
			</tr>
			
			
			
			<tr>
				<td>
					<?php echo $form->labelEx($model,'disc_code'); ?>
				</td>
				<td>
					<?php echo $form->textField($model,'disc_code',array('size'=>60,'maxlength'=>255,'class' => 'form-control input-sm')); ?>
					<?php echo $form->error($model,'disc_code'); ?>
				</td>
			</tr>
			
			<tr>
							<td><?php echo $form->labelEx($model,'period_start'); ?></td>
							<td>
							<?php
								$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									'model' => $model,
									//'value' => $model->sppd_id,
									'attribute' => 'period_start',
									'language' => 'en',
									'options' => array(
										//'dateFormat' => 'yy/mm/dd', --the real one
										'dateFormat' => 'yy-mm-dd',
										'showButtonPanel'=>true,
										'changeYear' => true,           // can change year
										'changeMonth' => true,          // can change month
										//'minDate'=>0,
										//'maxDate'=>"+1M +5D",
									),
									'htmlOptions' => array(
										'class' => 'form-control input-sm',
										'tabindex' => 2,
										'style'=>'width:350px'
									),
								));
							?>
							</td>
						</tr>
						
						<tr>
							<td><?php echo $form->labelEx($model,'period_end'); ?></td>
							<td>
							<?php
								$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									'model' => $model,
									//'value' => $model->sppd_id,
									'attribute' => 'period_end',
									'language' => 'en',
									'options' => array(
										//'dateFormat' => 'yy/mm/dd', --the real one
										'dateFormat' => 'yy-mm-dd',
										'showButtonPanel'=>true,
										'changeYear' => true,           // can change year
										'changeMonth' => true,          // can change month
										//'minDate'=>0,
										//'maxDate'=>"+1M +5D",
									),
									'htmlOptions' => array(
										'class' => 'form-control input-sm',
										'tabindex' => 2,
										'style'=>'width:350px'
									),
								));
							?>
							</td>
						</tr>
						
			<tr>
				<td>
					&nbsp;
				</td>
				<td>
					<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-success btn-sm')); ?>
				</td>
			</tr>
	</table>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
function commas(n) {
	
    n = n.replace(/,/g, '');
    var s = n.split('.')[1];
    (s) ? s = '.' + s : s = '';
    n = n.split('.')[0]
    while (n.length > 3) {
        s = ',' + n.substr(n.length - 3, 3) + s;
        n = n.substr(0, n.length - 3)
    }
    return n + s
}

$('input').keyup(function(){ // run anytime the value changes
    var price  = Number($('#price').val());   // get value of field
    var disc_rate = Number($('#disc_rate').val()); // convert it to a float
    var price  = Number($('#price').val());
    var disc_rate = Number($('#disc_rate').val());

    //$('#total_working_hours_weekdays').html(firstValue + secondValue + thirdValue + fourthValue); // add them and output it
    document.getElementById('price_after_disc').value = (1-(disc_rate/100)) *price;
	//document.getElementById('price_after_disc').value = weekends_working_days * weekends_working_hours;
// add them and output it
});

</script>