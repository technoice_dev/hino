

<h3>Dso Master Price Item</h3>



<?php

$dealer_id = DsoPartUpload::model()->get_dealer_id_sparepart(Yii::app()->user->id);

if($dealer_id != "HMSI")
{
?>
<br/><br/>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'dso-master-price-item-grid',
	'dataProvider'=>$model->searchbydealer(),
	'filter' => $model,
	'htmlOptions' => array('class' => 'table-responsive'),
		'itemsCssClass' => 'table table-striped table-bordered table-hover',
		'rowCssClassExpression' => '$row%2?"success":"even"',
		'pager' => array('class' => 'CLinkPager', 'header' => ''),
		'pagerCssClass' => 'pagination',
		'summaryCssClass' => 'dataTables_info',
	'columns'=>array(
		//'id',
		'part_code',
		'price',
		'top',
		'disc_rate',
		'price_after_disc',
		'period_start',
		'period_end',
		/*
		'period_start',
		'period_end',
		'created_by',
		'created_date',
		'modified_by',
		'modified_date',
		*/
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
<?php
} else
{
?>

<?php
		echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;&nbsp;Upload Price List',array('dsoMasterPriceItem/create_upload'),
         array('class'=>'btn btn-success btn-sm'));
		?>
		<?php
		echo CHtml::link('<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Price',array('dsoMasterPriceItem/create'),
         array('class'=>'btn btn-warning btn-sm'));
		?>
		<?php
		
				echo CHtml::link('</i>&nbsp;&nbsp;Delete Data by Upload Date',array('dsoMasterPriceItem/updatedata'),
				array('class' => 'btn btn-primary btn-sm'));
			
		?>

		<br/><br/>
		
		

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'dso-master-price-item-grid',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'htmlOptions' => array('class' => 'table-responsive'),
		'itemsCssClass' => 'table table-striped table-bordered table-hover',
		'rowCssClassExpression' => '$row%2?"success":"even"',
		'pager' => array('class' => 'CLinkPager', 'header' => ''),
		'pagerCssClass' => 'pagination',
		'summaryCssClass' => 'dataTables_info',
	'columns'=>array(
		//'id',
		'part_code',
		'price',
		'top',
		'disc_rate',
		'price_after_disc',
		'period_start',
		'period_end',
		'created_date',
		/*
		'period_start',
		'period_end',
		'created_by',
		'created_date',
		'modified_by',
		'modified_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php
}
?>