<?php
/* @var $this DsoMasterPriceItemController */
/* @var $model DsoMasterPriceItem */

$this->breadcrumbs=array(
	'Dso Master Price Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DsoMasterPriceItem', 'url'=>array('index')),
	array('label'=>'Create DsoMasterPriceItem', 'url'=>array('create')),
	array('label'=>'View DsoMasterPriceItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DsoMasterPriceItem', 'url'=>array('admin')),
);
?>

<h3>Update Price <?php echo $model->part_code; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>