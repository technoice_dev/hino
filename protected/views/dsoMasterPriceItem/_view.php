<?php
/* @var $this DsoMasterPriceItemController */
/* @var $data DsoMasterPriceItem */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('part_code')); ?>:</b>
	<?php echo CHtml::encode($data->part_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('top')); ?>:</b>
	<?php echo CHtml::encode($data->top); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disc_code')); ?>:</b>
	<?php echo CHtml::encode($data->disc_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_after_disc')); ?>:</b>
	<?php echo CHtml::encode($data->price_after_disc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('period_start')); ?>:</b>
	<?php echo CHtml::encode($data->period_start); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('period_end')); ?>:</b>
	<?php echo CHtml::encode($data->period_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_by')); ?>:</b>
	<?php echo CHtml::encode($data->modified_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_date')); ?>:</b>
	<?php echo CHtml::encode($data->modified_date); ?>
	<br />

	*/ ?>

</div>