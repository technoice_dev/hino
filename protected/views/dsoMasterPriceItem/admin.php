<?php
/* @var $this DsoMasterPriceItemController */
/* @var $model DsoMasterPriceItem */

$this->breadcrumbs=array(
	'Dso Master Price Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DsoMasterPriceItem', 'url'=>array('index')),
	array('label'=>'Create DsoMasterPriceItem', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#dso-master-price-item-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Dso Master Price Items</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'dso-master-price-item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'part_code',
		'price',
		'top',
		'disc_code',
		'price_after_disc',
		/*
		'period_start',
		'period_end',
		'created_by',
		'created_date',
		'modified_by',
		'modified_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
