<?php
/* @var $this DsoPartUploadController */
/* @var $model DsoPartUpload */
/* @var $form CActiveForm */
?>
<style>
<!--#customername{display:none;}-->
.customername {
    display:none;
}

.dealerid{
	display:none;
}
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dso-part-upload-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=> array('enctype'=>'multipart/form-data'),
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="panel panel-success">
		<div class="panel-body">
		
			<?php
				if(isset($_GET['id'])) $id = $_GET['id'];
			?>
			
			<?php if(Yii::app()->user->hasFlash('error')):?>
				<div class="alert alert-danger">
					<?php echo Yii::app()->user->getFlash('error'); ?>
				</div>
			<?php endif; ?>
			
			
			<div class="panel panel-default">
				<div class="panel-body">
					<table border="0" align="center" class="table-responsive">
						<?php //if ($model->isNewRecord) { ?>
						<tr>
							<td><b>Browse File</b></td>
							<td><?php echo $form->fileField($model, 'file_upload', array('size'=>60,'maxlength'=>200, 'disabled'=>false,'style'=>'width:350px')); ?></td>
						</tr>
						
						<tr>
							<td></td>
							<td>
								<?php 
								
									echo CHtml::submitbutton('Upload',
										array(
											'class' => 'btn btn-warning btn-sm',
											'id' => 'buttonRevise',
											'onclick'=>'return confirmationrevision()',
											)
									);
								

								?>
							</td>
						</tr>
						
						<h4><p class="note">Please attach in format .xls</p></h4>
					</table>
					
				</div>
			</div>
			<br/><br/>
			
			
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
function confirmationrevision()
{
	var answer = confirm("Are you sure ?");
	if (answer)
	{
		//document.getElementById("dso-part-upload-form").action=document.getElementById('urlimport').value;
		document.getElementById("dso-part-upload-form").submit();
		return true;
	} else {
		if (window.event) // True with IE, false with other browsers
		{
			window.event.returnValue=false; //IE specific
		} else {
			return false
		}
	}
}

</script>
<script>
var dlr_id = $("#dealer_id option:selected").val();
var path = "<?php echo $this->createUrl('DsoPartUpload/getBranch') ?> ";
$.ajax({  
type: "POST",      
url: path, //url to be called
data: { dealerID: dlr_id }, //data to be send
success: function( response ) {		

$('#branchname').html(response);
}
})
</script>