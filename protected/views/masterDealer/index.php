<?php
/* @var $this MasterDealerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Dealers',
);

$this->menu=array(
	array('label'=>'Create MasterDealer', 'url'=>array('create')),
	array('label'=>'Manage MasterDealer', 'url'=>array('admin')),
);
?>

<h1>Master Dealers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
