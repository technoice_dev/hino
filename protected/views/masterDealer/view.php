<?php
/* @var $this MasterDealerController */
/* @var $model MasterDealer */

$this->breadcrumbs=array(
	'Master Dealers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MasterDealer', 'url'=>array('index')),
	array('label'=>'Create MasterDealer', 'url'=>array('create')),
	array('label'=>'Update MasterDealer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MasterDealer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MasterDealer', 'url'=>array('admin')),
);
?>

<h1>View MasterDealer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'dealer_id',
		'dealer_name',
		'cabang_id',
		'facility',
		'dealer_type',
		'serial_code',
		'main_dealer',
		'region_id',
		'address',
		'no_phone',
		'fax',
		'establishment_date',
		'outlet_name',
		'sales_prefix',
		'website',
		'region_id_hcs',
	),
)); ?>
