<?php
/* @var $this MasterDealerController */
/* @var $data MasterDealer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealer_id')); ?>:</b>
	<?php echo CHtml::encode($data->dealer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealer_name')); ?>:</b>
	<?php echo CHtml::encode($data->dealer_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cabang_id')); ?>:</b>
	<?php echo CHtml::encode($data->cabang_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('facility')); ?>:</b>
	<?php echo CHtml::encode($data->facility); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealer_type')); ?>:</b>
	<?php echo CHtml::encode($data->dealer_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serial_code')); ?>:</b>
	<?php echo CHtml::encode($data->serial_code); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('main_dealer')); ?>:</b>
	<?php echo CHtml::encode($data->main_dealer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('region_id')); ?>:</b>
	<?php echo CHtml::encode($data->region_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_phone')); ?>:</b>
	<?php echo CHtml::encode($data->no_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fax')); ?>:</b>
	<?php echo CHtml::encode($data->fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('establishment_date')); ?>:</b>
	<?php echo CHtml::encode($data->establishment_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('outlet_name')); ?>:</b>
	<?php echo CHtml::encode($data->outlet_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sales_prefix')); ?>:</b>
	<?php echo CHtml::encode($data->sales_prefix); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('region_id_hcs')); ?>:</b>
	<?php echo CHtml::encode($data->region_id_hcs); ?>
	<br />

	*/ ?>

</div>