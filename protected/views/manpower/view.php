<?php
/* @var $this ManpowerController */
/* @var $model Manpower */

$this->breadcrumbs=array(
	'Manpowers'=>array('index'),
	$model->mp_id,
);

$this->menu=array(
	array('label'=>'List Manpower', 'url'=>array('index')),
	array('label'=>'Create Manpower', 'url'=>array('create')),
	array('label'=>'Update Manpower', 'url'=>array('update', 'id'=>$model->mp_id)),
	array('label'=>'Delete Manpower', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->mp_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Manpower', 'url'=>array('admin')),
);
?>

<h1>View Manpower #<?php echo $model->mp_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'mp_id',
		'bsc_sallary',
		'pph',
		'laundry',
		'transport',
		'meal',
		'medical',
		'mobile_phone',
		'bpjs',
		'thr',
		'uni_safety',
		'hardship',
		'overtime',
		'kategory',
		'id_dealer',
	),
)); ?>
