<?php
/* @var $this ManpowerController */
/* @var $model Manpower */

$this->breadcrumbs=array(
	'Manpowers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Manpower', 'url'=>array('index')),
	array('label'=>'Manage Manpower', 'url'=>array('admin')),
);
?>

<h1>Create Manpower</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>