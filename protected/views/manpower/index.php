<?php
/* @var $this ManpowerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manpowers',
);

$this->menu=array(
	array('label'=>'Create Manpower', 'url'=>array('create')),
	array('label'=>'Manage Manpower', 'url'=>array('admin')),
);
?>

<h1>Manpowers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
