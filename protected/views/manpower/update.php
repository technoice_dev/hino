<?php
/* @var $this ManpowerController */
/* @var $model Manpower */

$this->breadcrumbs=array(
	'Manpowers'=>array('index'),
	$model->mp_id=>array('view','id'=>$model->mp_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Manpower', 'url'=>array('index')),
	array('label'=>'Create Manpower', 'url'=>array('create')),
	array('label'=>'View Manpower', 'url'=>array('view', 'id'=>$model->mp_id)),
	array('label'=>'Manage Manpower', 'url'=>array('admin')),
);
?>

<h1>Update Manpower <?php echo $model->mp_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>