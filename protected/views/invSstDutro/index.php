<?php
/* @var $this InvSstDutroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inv Sst Dutros',
);

$this->menu=array(
	array('label'=>'Create InvSstDutro', 'url'=>array('create')),
	array('label'=>'Manage InvSstDutro', 'url'=>array('admin')),
);
?>

<h1>Inv Sst Dutros</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
