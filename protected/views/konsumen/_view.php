<?php
/* @var $this KonsumenController */
/* @var $data Konsumen */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_konsumen')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_konsumen), array('view', 'id'=>$data->id_konsumen)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_telp')); ?>:</b>
	<?php echo CHtml::encode($data->no_telp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />


</div>