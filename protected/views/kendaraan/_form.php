<?php
/* @var $this KendaraanController */
/* @var $model Kendaraan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kendaraan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Field dengan Tanda <span class="required">*</span> Wajib Diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'no_kendaraan'); ?>
		<div class="col-sm-10">
		<?php echo $form->textField($model,'no_kendaraan'); ?>
		<?php echo $form->error($model,'no_kendaraan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tipe_kendaraan'); ?>
		<div class="col-sm-10">
		<?php echo $form->textField($model,'tipe_kendaraan',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'tipe_kendaraan'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'km_kendaraan'); ?>
		<div class="col-sm-10">
		<?php echo $form->textField($model,'km_kendaraan',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'km_kendaraan'); ?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->