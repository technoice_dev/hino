<?php


/**

 * UserIdentity represents the data needed to identity a user.

 * It contains the authentication method that checks if the provided

 * data can identity the user.

 */

class UserIdentity extends CUserIdentity {
	/**
	 * @var integer id of logged user
	 */
	private $_id;
	private $_first_name;
	public $level;
	
	
	//Must need to add
	public function __construct($username, $password, $level)
	{
		$this->username = $username;
		$this->password = $password;
		
	}


	/**
	 * Authenticates username and password
	 * @return boolean CUserIdentity::ERROR_NONE if successful authentication
	 */
	public function authenticate() {
		
	
		$login = null;
		$login = User::model()->findByAttributes(['username'=>$this->username]);
		
		//Login Authentication & Validation
		if ($login === null) {
		         
				$this->errorCode = self::ERROR_USERNAME_INVALID;
		} 
	    
		else if ($login->password!= md5($this->password)) {
                        
		 		$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}

		 else {
			$this->_id = $login->id_user;
                        $this->setState('username', $this->username);
			$this->setState('level', $login->level);
                        $this->setState('id_user', $login->id_user);
                        $this->setState('dealer', $login->id_dealer);

			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}


	/**
	 *
	 * @return integer id of the logged user, null if not set
	 */
	public function getId() {
		return $this->_id;
	}
	
	public function getFirstName() {
		return $this->_first_name;
	}

}
?>