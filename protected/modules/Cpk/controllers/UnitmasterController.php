<?php

class UnitmasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	//Hak akses untuk menu Unitmaster
	public function accessRules()
	{
            $level = isset(Yii::app()->user->level) ? Yii::app()->user->level : 'c';
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','delete'),
				'expression'=>'Yii::app()->user->level =="super" ',
			),
                        array('deny',  // deny all users
				'expression'=>'Yii::app()->user->isGuest ',
			),
			array('deny',  // deny all users
				'expression'=> '"'.$level.'" !== "super" ',
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	//Function untuk Detail Unitmaster
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	 //Function Untuk Menampilkan Create Unitmaster
	public function actionCreate()
	{
		$model=new Unitmaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//Ketika Dibuka menggunakan method GET maka akan menampilkan form create Unitmaster
		//Ketika Mengirim Request Menggunakan POST maka akan masuk ke insert Unitmaster

		if(isset($_POST['Unitmaster']))
		{
					$model->attributes=$_POST['Unitmaster'];
					$unit_name = $model->unit_name;
					$unit_type = $model->unit_type;
					$unitData = UnitMaster::model()->findAllByAttributes(['unit_name' => $unit_name]);
					$unitTypeData = UnitMaster::model()->findAllByAttributes(['unit_type' => $unit_type]);
					
					if(isset($unitData) || isset($unitTypeData)){
						if (count($unitData) > 0){
							Yii::app()->user->setFlash('error', "error");
							if (count($unitData) > 0){
								Yii::app()->user->setFlash('unit_name', "<strong>Nama Unit</strong> sudah ada");	
							}
							//Validasi data unit
							//ketika data unit sudah ada maka akan redirect kembali ke form create unit
							$this->redirect(Yii::app()->request->urlReferrer);
						}
						
					}
					//Save Data Unit Master
                    if($model->save()){
						//Ketika berhasil maka akan redirect ke detail unit
                        Yii::app()->user->setFlash('success', "Data berhasil diubah");
                        $this->redirect(array('view','id'=>$model->unit_id));
                    } else {
						//Ketika gagal validasi data maka akan redirect kembali ke form create UnitMaster
                        Yii::app()->user->setFlash('error', "error");
                        if($model->unit_type == null){
                            Yii::app()->user->setFlash('unit_type', "<strong>Unit Type</strong> tidak boleh kosong");
                        }
                        if($model->unit_name == null){
                            Yii::app()->user->setFlash('unit_name', "<strong>Unit Name</strong> tidak boleh kosong");
                        }
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	//Function Update UnitMaster
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//Validasi Request , ketika request GET maka akan menampilkan form update unitmaster 
		//ketika Request POST dan mempunyai data Unitmaster maka akan masuk ke proses update unitmaster
		if(isset($_POST['Unitmaster']))
		{
					$model->attributes=$_POST['Unitmaster'];
                    if($model->save()){
						//Ketika berhasil save unitmaster maka akan redirect ke detail unit master
                        Yii::app()->user->setFlash('success', "Data berhasil diubah");
                        $this->redirect(array('view','id'=>$model->unit_id));
                    } else {

						//ketika gagal validasi maka akan di kembalikan ke form update unit master
                        Yii::app()->user->setFlash('error', "error");
                        if($model->unit_type == null){
                            Yii::app()->user->setFlash('unit_type', "<strong>Unit Type</strong> tidak boleh kosong");
                        }
                        if($model->unit_name == null){
                            Yii::app()->user->setFlash('unit_name', "<strong>Unit Name</strong> tidak boleh kosong");
                        }
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */

	 //Function Delete unitmaster
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete(); // Mencari data unitmaster dan menghapus nya

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
						//ketika berhasil maka akan redirect ke index unitmaster
                        Yii::app()->user->setFlash('success', "Data berhasil dihapus");
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	//List Data unitmaster
	public function actionIndex()
	{
			//Inisialisasi kriteria database
            $criteria = new CDbCriteria();
            $search = Yii::app()->request->getQuery('search') ? Yii::app()->request->getQuery('search') : null;
			//cek jika ada filter search
            if($search){
				//jika ada filter search maka akan mencari data ke kolom yang ada di tabel unit master
                $criteria->addCondition('unit_type like "%'.urldecode($search).'%" ','OR');
                $criteria->addCondition('unit_name like "%'.urldecode($search).'%" ','OR');
                
			}

			//hitung data hasil filter
            $count = Unitmaster::model()->with('partbytype','invDet')->count($criteria);
            
            $pages = new CPagination($count);
            $pages->pageSize=10;
            $pages->applyLimit($criteria);
			//inisialisasi plugin paging bawaan yii

			//Mengambil data setelah cek filter
            $model = Unitmaster::model()->with('partbytype','invDet')->findAll($criteria);
            $this->render('index',array(
		'model'=>$model,
                'pages'=>$pages,
                'search'=>$search,
            ));
	}

	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Unitmaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Unitmaster']))
			$model->attributes=$_GET['Unitmaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Unitmaster the loaded model
	 * @throws CHttpException
	 */
	//Function untuk load data berdasarkan primary key
	public function loadModel($id)
	{
		
		$model=Unitmaster::model()->findByPk($id); // mencari data berdasarkan primary key
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.'); // jika data tidak ditemukan maka akan return status 404
		return $model; // jika data ditemukan maka akan return data unitmaster
	}

	/**
	 * Performs the AJAX validation.
	 * @param Unitmaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='unitmaster-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
