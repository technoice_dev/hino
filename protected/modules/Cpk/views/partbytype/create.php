<?php
/* @var $this PartbytypeController */
/* @var $model Partbytype */

$this->breadcrumbs=array(
	'Partbytypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Partbytype', 'url'=>array('index')),
	array('label'=>'Manage Partbytype', 'url'=>array('admin')),
);
?>

<h1>Create Partbytype</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>