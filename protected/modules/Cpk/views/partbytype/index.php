<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Part By Type',
);


?>

<div class="row">
    <h2>Daftar Part</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/partbytype/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/partbytype/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>
<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="table-responsive">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <?php
                        $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/partbytype?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/partbytype?';
                        $sortByUnit = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'unit.unit_name' ?  $_GET['sortBy'] : 'unit.unit_name';
                        $sortByPartName = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'part.part_name' ?  $_GET['sortBy'] : 'part.part_name';
                        $sortByPartNumber = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'part.part_no' ?  $_GET['sortBy'] : 'part.part_no';
                        $sortByQty = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'qty' ?  $_GET['sortBy'] : 'qty';
                        $sortByKmInterval = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'kminterval' ?  $_GET['sortBy'] : 'kminterval';
                        $sortByProbabilitas = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'probabilitas' ?  $_GET['sortBy'] : 'probabilitas';
                        $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                    ?>
                    <td style="text-align:center;">No</td>
                    <td><a href="<?= $url.'sortBy='.$sortByUnit.'&sortType='.$sortType?>">Unit</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByPartName.'&sortType='.$sortType?>">Part</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByPartNumber.'&sortType='.$sortType?>">Part Number</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByQty.'&sortType='.$sortType?>">Qty</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByKmInterval.'&sortType='.$sortType?>">Km Interval</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByProbabilitas.'&sortType='.$sortType?>">Probabilitas</a></td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= (isset($row->unit->unit_name) ? $row->unit->unit_name : '') ?></td>
                        <td><?= (isset($row->part->part_name) ? $row->part->part_name : '') ?></td>
                        <td><?= (isset($row->part->part_no) ? $row->part->part_no : '') ?></td>
                        <td style="text-align:right;"><?= $row->qty?></td>
                        <td style="text-align:right;"><?= number_format($row->kminterval,0,'','.') ?></td>
                        <td style="text-align:right;"><?= $row->probabilitas?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/partbytype/update/id/<?= $row->typemtc_id?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <button data-val="<?= $row->typemtc_id?>" class="btn btn-danger btn-sm del-button">Hapus</button>
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/partbytype/view/id/<?= $row->typemtc_id?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>

<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/partbytype/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>