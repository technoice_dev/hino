<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<div class="card">
    <div class="card-body">
        <div class="form">
            
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'partbytype-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Unit',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <select name="Partbytype[unit_id]" id="unit" class="form-control">
                                <option value="">Pilih Unit</option>
                                <?php
                                    if (isset($dataUnit) && count($dataUnit) > 0){
                                        foreach ($dataUnit as $du){
                                ?>
                                    <option value="<?=$du->unit_id?>" selected><?=$du->unit_name ?></option>
                                        <?php }
                                    } ?>
                            </select>
                        </div>
                        
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Part',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php //echo $form->dropDownList($model, 'part_id', $optionPart,array('class'=>'form-control')) ?>
                            <select id="part" name="Partbytype[part_id]" class="form-control select2">
                                <option value="">- pilih Part -</option>
                                <?php
                                    if (isset($dataPart) && count($dataPart) > 0){
                                        foreach ($dataPart as $dp){
                                ?>
                                    <option value="<?=$dp->part_id?>" selected><?=$dp->part_name ?></option>
                                        <?php }
                                    } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Qty',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'qty',array('class'=>'form-control km-field','id'=>'pricelist','type'=>'text')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Km Interval',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->numberField($model,'kminterval',array('class'=>'form-control','id'=>'km1th','type'=>'')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Probabilitas',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'probabilitas',array('class'=>'form-control km-field','id'=>'id_dealer','type'=>'text')); ?>
                        </div>
                    </div>
                    <br>    
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            
        </div><!-- form -->
    </div>
</div>

<script type="text/javascript">
var url = '<?= Yii::app()->baseUrl?>/Cpk/partbytype/getPart';
var url_unit = '<?= Yii::app()->baseUrl?>/Cpk/partbytype/getUnit';
$(function () {
    $("#part").select2({
        allowClear: true,
        placeholder: 'masukkan nama part',
        ajax: {
            dataType: 'json',
            url: url,
            data: function (params) {
                return {
                    search: params.term
                }
            },
            processResults: function (data, page) {
                return {
                    results : data
                };
            },
        }
    });
    $("#unit").select2({
        allowClear: true,
        placeholder: 'masukkan nama unit',
        ajax: {
            dataType: 'json',
            url: url_unit,
            data: function (params) {
                return {
                    search: params.term
                }
            },
            processResults: function (data, page) {
                return {
                    results : data
                };
            },
        }
    });
});
</script>