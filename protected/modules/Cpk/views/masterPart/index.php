<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Parts',
);


?>

<div class="row">
    <h2>Daftar Part</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/masterPart/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/masterPart/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <select name="category" class="form-control">
                        <option value="">All</option>
                        <option value="periodic" <?= 'periodic' == $category ? 'selected' : ''?>>Periodic</option>
                        <option value="preventive" <?= 'preventive' == $category ? 'selected' : ''?>>Preventive</option>
                    </select>
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>
<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <?php
                        $url = isset($_GET['search']) || isset($_GET['category']) ? Yii::app()->baseUrl.'/Cpk/masterPart?search='.$_GET['search'].'&category='.$_GET['category'].'&' : Yii::app()->baseUrl.'/Cpk/masterPart?';
                        $sortByPartName = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'part_name' ?  $_GET['sortBy'] : 'part_name';
                        $sortByPartNo = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'part_no' ?  $_GET['sortBy'] : 'part_no';
                        $sortByPricelist = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'pricelist' ?  $_GET['sortBy'] : 'pricelist';
                        $sortByKm1th = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'km1th' ?  $_GET['sortBy'] : 'km1th';                            $sortByCategory = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'category' ?  $_GET['sortBy'] : 'category';
                        $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                    ?>
                    <td style="text-align:center;">ID Part</td>
                    <td><a href="<?= $url.'sortBy='.$sortByPartName.'&sortType='.$sortType?>">Nama Part</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByPartNo.'&sortType='.$sortType?>">No Part</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByPricelist.'&sortType='.$sortType?>">Harga Part</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByCategory.'&sortType='.$sortType?>">Part Category</a></td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {?>
                    <tr>    
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= $row->part_name?></td>
                        <td><?= $row->part_no?></td>
                        <td style="text-align:right;"><?= number_format($row->pricelist,0,',','.') ?></td>
                        <td><?= $row->category == 'preventive'? 'Preventive' : 'Periodic' ?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/masterPart/update/id/<?= $row->part_id?>" class="btn btn-primary btn-sm">Ubah</a> 
                        <a href="<?= Yii::app()->baseUrl ;?>/Cpk/masterPart/view/id/<?= $row->part_id?>" class="btn btn-info btn-sm">View</a>
                            <?php if(count($row->partbytype) > 0 || count($row->invoiceDetPart) > 0) {
                                
                            } else {
                            ?>
                            <button data-val="<?= $row->part_id?>" class="btn btn-danger btn-sm del-button">Hapus</button>

                            <?php } ?>
                            </td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>


<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/masterPart/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>
