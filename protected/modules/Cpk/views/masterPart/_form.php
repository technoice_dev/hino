<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
?>
<style type="text/css">
    .invalid{
        outline-color: red;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="form">
            
                <?php $form=$this->beginWidget('CActiveForm', array(
                       'id'=>'master-part-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                )); ?>

                    <?php if(Yii::app()->user->getFlash('error')){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        echo '<span aria-hidden="true">&times;</span>';
                        echo '</button>';
                        foreach(Yii::app()->user->getFlashes() as $key => $value){
                            echo $value.'</br>';
                        }
                        
                        echo '</div>';
                        
                    } ?>
                    
                    
                    
                    <?php echo $form->errorSummary($model); ?>
                    
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Nama Part',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'part_name',array('size'=>60,'maxlength'=>100,'class'=>'form-control','id'=>'part_name')); ?>
                        </div>
                        
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Nomor Part',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->textField($model,'part_no',array('size'=>20,'maxlength'=>20,'class'=>'form-control','id'=>'part_no','type'=>'number')); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Harga',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->numberField($model,'pricelist',array('class'=>'form-control numberic-field','id'=>'pricelist','type'=>'number')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php echo $form->labelEx($model,'Category',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-6">
                            <?php echo $form->dropDownlist($model,'category',array('periodic'=>'Periodic','preventive'=>'Preventive'),array('class'=>'form-control')); ?>
                        </div>
                    </div>
                    <br>    
                    <div class="form-group row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            
        </div><!-- form -->
    </div>
</div>
