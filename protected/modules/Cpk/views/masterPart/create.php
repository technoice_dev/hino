<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */

$this->breadcrumbs=array(
	'Master Parts'=>array('index'),
	'Create',
);


?>

<h3>Tambah Part Baru</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>