<?php
/* @var $this InvSstRangerController */
/* @var $model InvSstRanger */

$this->breadcrumbs=array(
	'Inv Sst Rangers'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvSstRanger', 'url'=>array('index')),
	array('label'=>'Create InvSstRanger', 'url'=>array('create')),
	array('label'=>'View InvSstRanger', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage InvSstRanger', 'url'=>array('admin')),
);
?>

<h1>Update InvSstRanger <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>