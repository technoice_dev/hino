<?php
/* @var $this InvMeasuringController */
/* @var $model InvMeasuring */

$this->breadcrumbs=array(
	'Investment'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InvMeasuring', 'url'=>array('index')),
	array('label'=>'Manage InvMeasuring', 'url'=>array('admin')),
);
?>

<h1>Add Investment</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>