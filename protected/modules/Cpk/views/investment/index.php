<?php
/* @var $this InvestmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Investment',
);

$this->menu=array(
	array('label'=>'Create Investment', 'url'=>array('create')),
	array('label'=>'Manage Investment', 'url'=>array('admin')),
);
?>

<div class="row">
    <h1>Investment</h1>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/investment/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/investment/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>
<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>


<?php
    
        $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/investment?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/investment?';
    
    
    $sortByInvId = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'inv_id' ?  $_GET['sortBy'] : 'inv_id';
    $sortByInvItem = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'inv_item' ?  $_GET['sortBy'] : 'inv_item';
    $sortByInvPrice = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'inv_price' ?  $_GET['sortBy'] : 'inv_price';
    $sortByInvQty = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'inv_qty' ?  $_GET['sortBy'] : 'inv_qty';
    $sortByInvCategory = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'inv_category' ?  $_GET['sortBy'] : 'inv_category';
    $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
    
    
    
?>
<div class="row">
    <div class="table-responsive">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <td style="text-align:center;"><a href="<?= $url.'sortBy='.$sortByInvId.'&sortType='.$sortType?>">No</a></td>
                    <td style="text-align:center;"><a href="<?= $url.'sortBy='.$sortByInvItem.'&sortType='.$sortType?>">Investment Item</a></td>
                    <td style="text-align:center;"><a href="<?= $url.'sortBy='.$sortByInvPrice.'&sortType='.$sortType?>">Investment Price</a></td>
                    <td style="text-align:left;"><a href="<?= $url.'sortBy='.$sortByInvQty.'&sortType='.$sortType?>">Investment Qty</a></td>
                    <td style="text-align:left;"><a href="<?= $url.'sortBy='.$sortByInvId.'&sortType='.$sortType?>">Investment Category</a></td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= $row->inv_item?></td>
                        <td style="text-align:center;"><?= number_format($row->inv_price,0,',','.')?></td>
                        <td style="text-align:left;"><?= $row->inv_qty?></td>
                        <td style="text-align:left;"><?= $row->inv_category?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/investment/update/id/<?= $row->inv_id?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <button data-val="<?= $row->inv_id?>" class="btn btn-danger btn-sm del-button">Hapus</button> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/investment/view/id/<?= $row->inv_id?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>

<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/investment/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>