<?php
/* @var $this InvestmentController */
/* @var $model Investment */

$this->breadcrumbs=array(
	'Investment'=>array('index'),
	$model->inv_id,
);

$this->menu=array(
	array('label'=>'List Investment', 'url'=>array('index')),
	array('label'=>'Create Investment', 'url'=>array('create')),
	array('label'=>'Update Investment', 'url'=>array('update', 'id'=>$model->inv_id)),
	array('label'=>'Delete Investment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->inv_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Investment', 'url'=>array('admin')),
);
?>

<h1>View Investment</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/investment" class="btn btn-primary">Investment List</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Investment ID</td>
                <td><?= $model->inv_id ?></td>
            </tr>
            <tr>
                <td>Investment Item</td>
                <td><?= $model->inv_item ?></td>
            </tr>
            <tr>
                <td>Invesment Price</td>
                <td><?= number_format($model->inv_price,0,',','.') ?></td>
            </tr>
            <tr>
                <td>Investment Qty</td>
                <td><?= $model->inv_qty ?></td>
            </tr>
            <tr>
                <td>Investment Category</td>
                <td><?= $model->inv_category ?></td>
            </tr>
        </table>
    </div>
</div>
