<?php
/* @var $this KendaraanController */
/* @var $model Kendaraan */

$this->breadcrumbs=array(
	'Kendaraans'=>array('index'),
	$model->no_kendaraan=>array('view','id'=>$model->no_kendaraan),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kendaraan', 'url'=>array('index')),
	array('label'=>'Tambah Kendaraan', 'url'=>array('create')),
	array('label'=>'Lihat Kendaraan', 'url'=>array('view', 'id'=>$model->no_kendaraan)),
	array('label'=>'Atur Kendaraan', 'url'=>array('admin')),
);
?>

<h1>Update Kendaraan <?php echo $model->no_kendaraan; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>