<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'List Penawaran',
);


?>

<div class="row">
    <h2>List Penawaran</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {   
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/penawaran/invList" method="get">
            
                <div class="form-group mx-sm-3 mb-2">
                    <input type="hidden" name="invID" value="<?= $_GET['invID']?>">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>

<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="table-responsive">
        <table class="table table-stripped table-bordered">
            <thead>
                <tr>
                    <td rowspan="2" style="text-align:center;">No</td>
                    <td rowspan="2">Unit</td>
                    <td rowspan="2">Kontrak(tahun)</td>
                    <td rowspan="2">Km/hari</td>
                    <td rowspan="2">Diskon</td>
                    <td colspan="4" style="text-align:center;">Cost</td>
                    <td rowspan="2">Junior</td>
                    <td rowspan="2">Senior</td>
                    <td rowspan="2">Foreman</td>
                    <td rowspan="2">Staff W/H</td>
                    <td rowspan="2">Leader</td>
                    <td rowspan="2">Other</td>
                    <td rowspan="2">Work Day</td>
                    <td rowspan="2">Unit Qty</td>
                    <td rowspan="2" style="text-align:center;">Action</td>
                    
                </tr>
                <tr>
                    <td>Preventive</td>
                    <td>Periodic</td>
                    <td>Labor</td>
                    <td>Invest</td>
                </tr>
            </thead>
            <tbody>

                <?php $i = 1; foreach($data as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= isset($row->unit->unit_name) ? $row->unit->unit_name : ''  ?></td>
                        <td><?= $row->kontrak?></td>
                        <td><?= $row->km_day?></td>
                        <td><?= $row->discount?></td>
                        <td><?= number_format($row->preventive_cost,0,',','.')?></td>
                        <td><?= number_format($row->periodic_cost,0,',','.')?></td>
                        <?php if ($i == 1){?>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= number_format($invoice->labor_cost,0,',','.')?></td>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= number_format($invoice->invest_cost,0,',','.')?></td>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= $invoice->jml_senior?></td>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= $invoice->jml_junior?></td>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= $invoice->jml_foreman?></td>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= $invoice->jml_staff?></td>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= $invoice->jml_leader?></td>
                            <td style="vertical-align:middle;" rowspan="<?= $pages->pageSize ?>"><?= $invoice->jml_other?></td>
                        <?php } ?>
                        <td><?= $row->work_day?></td>
                        <td><?= $row->unit_qty?></td>
                        <td style="text-align:center;">
                            <a class="btn btn-success" href="<?= Yii::app()->baseUrl; ?>/Cpk/penawaran/cpkDetail?idPenawaran=<?=$row->id?>">Detail Penawaran</a>
                            <!-- <a class="btn btn-danger" href="<?= Yii::app()->baseUrl; ?>/Cpk/penawaran/deleteInvDet?idPenawaran=<?=$row->id?>">Hapus Penawaran</a> -->
                            <button data-val="<?= $row->id?>" class="btn btn-danger btn-sm del-button">Hapus Penawaran</button>
                        </td>
                    </tr>
            
                <?php $i++; } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>



<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/penawaran/deleteInvDet/idPenawaran/'+delete_data;
    window.location.href = delete_url;
});

</script>