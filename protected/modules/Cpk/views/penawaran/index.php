<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Penawaran',
);


?>

<div class="row">
    <h2>Penawaran</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {   
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus Penawaran ini ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="">
        <div class="col pull-left">
            
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/penawaran/index" method="get" autocomplete="off">
                <div class="form-group mx-sm-3 mb-2">
                    <!-- <input type="text" name="from_date" class="form-control" value="<?= $from?>" id="date-from">        -->
                    <!-- <input type="text" name="to_date" class="form-control" value="<?= $to?>" id="date-to">      -->
                    Dari Tgl
                    <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                            'name'=>'from_date',
                            'value' => $from,
                            'options'=>array(
                                'dateFormat' => 'yy-mm-dd',
                                'showAnim'=>'slideDown',
                                
                            ),
                            'htmlOptions'=>array(
                                'class'=>'form-control',
                                'size'=>'25',
                                'style'=>'',
                                
                            ),
                        ));
                    ?>
                    &nbsp; Sampai Tgl &nbsp;
                    <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                            'name'=>'to_date',
                            'value' => $to,
                            'options'=>array(
                                'dateFormat' => 'yy-mm-dd',
                                'showAnim'=>'slideDown',
                            ),
                            'htmlOptions'=>array(
                                'class'=>'form-control',
                                'size'=>'25',
                                'style'=>'',
                            ),
                        ));
                    ?>
                </div>
                <!-- <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div> -->
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>

<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="table-responsive">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <?php
                        $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/penawaran?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/penawaran?';
                        $sortByCustomer = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'customer.cust_name' ?  $_GET['sortBy'] : 'customer.cust_name';
                        $sortByInvoice = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'id_invoice' ?  $_GET['sortBy'] : 'id_invoice';
                        $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                    ?>
                    <td style="text-align:center;">No</td>
                    <td>Customer Name</td>
                    <td>ID Invoice</td>
                    <td>Invoice Date</td>
                    <td style="text-align:center;">Action</td>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {
                        if(count($row->invDet) > 0 ){
                    ?>
                    
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= isset($row->customer->cust_name) ? $row->customer->cust_name : '' ?></td>
                        <td><?= $row->id_invoice?></td>
                        <td><?= $row->tgl?></td>
                        <td style="text-align:center;">
                            <a href="<?= Yii::app()->baseUrl?>/Cpk/default/cpkCalculate?id_invoice=<?=$row->id_invoice ?>" class="btn btn-success">Tambah Penawaran</a>
                            <a href="<?= Yii::app()->baseUrl?>/Cpk/default/printInvoice?invID=<?=$row->id_invoice ?>" class="btn btn-primary btn-sm">Print Invoice</a>                         
                            <a class="btn btn-info" href="<?= Yii::app()->baseUrl; ?>/Cpk/penawaran/invList?invID=<?=$row->id_invoice?>">List Penawaran</a>
                            <button data-val="<?= $row->id_invoice?>" class="btn btn-danger btn-sm del-button">Hapus</button> 
                            <a href="<?= Yii::app()->baseUrl?>/Cpk/default/editCost?invID=<?=$row->id_invoice ?>" class="btn btn-warning btn-sm">Manpower dan Investment</a>
                        </td>
                    </tr>
            
                <?php }
                } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>
<script>
    $('#date-from').datepicker({
        format : 'yyyy-mm-dd',
    });
    $('#date-to').datepicker({
        format : 'yyyy-mm-dd',
    });
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/penawaran/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>

