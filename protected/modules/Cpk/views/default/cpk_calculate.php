<?php
/* @var $this MasterPartController */
/* @var $model MasterPart */
/* @var $form CActiveForm */
$session = Yii::app()->session;
                    
?>
<style type="text/css">
    

    .input-group{
        display: table;
        border-collapse: collapse;
        width:100%;
    }
    .input-group > div{
        display: table-cell;
        border: 1px solid #ddd;
        vertical-align: middle;  /* needed for Safari */
    }
    .input-group-icon{
        background:#eee;
        color: #777;
        padding: 0 12px
    }
    .input-group-area{
         width:100%;
    }
    .input-group input{
        border: 0;
        display: block;
        width: 100%;
        padding: 9px;
    }
    .center-floating{
        float: get_current_user;
    }

</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<form action="<?= Yii::app()->baseUrl; ?>/Cpk/default/process" method="POST" id="cpk-simulasi">
    <div class="card center-floating">
        <div class="card-body">
            <div class="form">
                <h1>Perhitungan CPK</h1>
                    

                        <?php
                        
                        if(Yii::app()->user->getFlash('error')){
                            echo '<div class="alert alert-danger" role="alert">';
                            echo '<button type="button" class="close" data-dimdiss="alert" aria-label="Close">';
                            echo '<span aria-hidden="true">&times;</span>';
                            echo '</button>';
                            foreach(Yii::app()->user->getFlashes() as $key => $value){
                                echo $value.'</br>';
                            }
                            
                            echo '</div>';
                            
                        } ?>
                        
                        
                        <div class="row">
                        <div class="panel panel-default col-md-8">
                            <div class="panel-body">           
                        
                                <div class="form-group row">
                                    <div class="col col-md-2">
                                        <label>Discount</label>
                                    </div>
                                    <div class="col col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-area">
                                                <input type="number" class="form-control" min="0" max="100" name="disc" value="0">
                                            </div>
                                            <div class="input-group-icon">%</div>
                                        </div>
                                        <span id="error-disc" style="color:red;"></span>
                                    </div>                                                   
                                </div>

                                <div class="form-group row">
                                    <div class="col col-md-2">
                                        <label>Type</label>
                                    </div>
                                    <div class="col col-md-9">    
                                        <select class="form-control" name="tipe" id="unit">
                                            <?php foreach($unit as $row){
                                                if(isset($row->partbytype) && count($row->partbytype) > 0) {?>
                                                <option value="<?= $row->unit_id?>"><?= $row->unit_name.' ('.$row->unit_type.')'?></option>
                                                <?php }
                                        } ?>
                                        </select>
                                        <span id="error-tipe" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="form-group form row">
                                    <div class="col col-md-2">
                                        <label>Jumlah Unit</label>
                                    </div>
                                    <div class="col col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-area">
                                                <input type="number" class="form-control" name="jumlah" value="0">
                                            </div>
                                            <div class="input-group-icon">Kendaraan</div>
                                        </div>                     
                                        <span id="error-jumlah" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col col-md-2">
                                        <label>Jarak Tempuh / hari </label>
                                    </div>
                                    <div class="col col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-area">
                                                <input type="text" class="form-control km-field" name="jarak" value="0" class="number">
                                            </div>
                                            <div class="input-group-icon">Km</div>
                                        </div> 
                                        <span id="error-jarak" style="color:red;"></span>
                                    </div>
                                </div>
                                <?php if(!isset($_SESSION['customer'])) {?>
                                    <div class="form-group row">
                                        <div class="col col-md-2">
                                            <label>Customer</label>
                                        </div>
                                        <div class="col col-md-9">
                                            <select class="form-control" name="customer" id="customer">
                                                <?php foreach($customer as $row){?>
                                                    <option value="<?= $row->id_cust?>"><?= $row->cust_name.' ('.$row->phone.')'?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <span id="error-customer" style="color:red;"></span>
                                    </div>
                                <?php } ?>
                                <div class="form-group row">
                                    <div class="col col-md-2">
                                        <label>Working Days</label>
                                    </div>
                                    <div class="col col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-area">                                
                                                <input type="number" class="form-control" name="day" value="0">
                                            </div>
                                            <div class="input-group-icon">Max 30</div>
                                        </div>  
                                        <span id="error-day" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label>Lama Kontrak</label>
                                    </div>
                                    <div class="col col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-area">                                
                                                <input type="number" class="form-control" name="kontrak" value="0">
                                            </div>
                                            <div class="input-group-icon">Tahun</div>
                                        </div>
                                        <span id="error-kontrak" style="color:red;"></span>                     
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                            
                        

                    
                
            </div><!-- form -->
        </div>
    </div>
</form>
<div class="form-group row buttons">
    <button type="submit" class="btn btn-success">Submit</button>
    <?php if($session['invId'] !== null ) {?>
        <a class="btn btn-info" href="<?= Yii::app()->baseUrl ?>/Cpk/default/destroyinvoice">Simpan dan buat transaksi baru</a>
    <?php } ?>
</div>
<script>
    var disc = $('[name="disc"]').val();
    var tipe = $('[name="tipe]').val();
    var jumlah = $('[name="jumlah"').val();
    var jarak = $('[name="jarak"]').val();
    var day = $('[name="day"]').val();
    var kontrak = $('[name="kontrak"]').val();
    var senior = $('[name="senior"]').val();
    var junior = $('[name="junior"]').val();
    var foreman = $('[name="foreman"]').val();
    var staffWh = $('[name="staffWh"]').val();
    var leader = $('[name="leader"]').val();
    var other = $('[name="other"]').val();
    var customer = $('[name="customer"]').val();
    var url_unit = '<?= Yii::app()->baseUrl?>/Cpk/default/getUnit';
    var url_customer = '<?= Yii::app()->baseUrl?>/Cpk/default/getCustomer';
    $("#unit").select2({
        allowClear: true,
        placeholder: 'masukkan nama customer',
        ajax: {
            dataType: 'json',
            url: url_unit,
            data: function (params) {
                return {
                    search: params.term
                }
            },
            processResults: function (data, page) {
                return {
                    results : data
                };
            },
        }
    });

    $("#customer").select2({
        allowClear: true,
        placeholder: 'masukkan nama customer',
        ajax: {
            dataType: 'json',
            url: url_customer,
            data: function (params) {
                return {
                    search: params.term
                }
            },
            processResults: function (data, page) {
                return {
                    results : data
                };
            },
        }
    });

</script>