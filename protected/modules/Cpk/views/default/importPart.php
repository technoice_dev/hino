
    
<?php 
    $success = Yii::app()->user->getFlash('success');
    if ($success !== null){
?>
<div id="base-popup">
    <div class="modal in" id="myModal" role="dialog" style="display: block; background-color:rgba(0, 0, 0, 0.22);">
        <div class="modal-dialog">
        
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
            <p>Import Data Part Berhasil</p>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="close-popup">Close</button>
            </div>
        </div>
        
        </div>
    </div>
  </div>
  <?php } ?>

<div class="row">
    <div class="col">
        <form action="<?= Yii::app()->baseUrl?>/Cpk/default/importDataPart" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="file-input">File</label>
                <input type="file" name="excel_data" class="form-control" id="file-input">
                <a href="<?= Yii::app()->baseUrl; ?>/doc/import_data_part.xlsx" class="btn btn-primary" download>Download File Contoh Part</a>
                <input type="submit" value="Import" class="btn btn-success">
            </div>
        </form>
    </div>
</div>

<script>
    $('#close-popup').click(function(){
        $('#base-popup').empty();
    });
</script>