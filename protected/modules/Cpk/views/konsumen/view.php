<?php
/* @var $this KonsumenController */
/* @var $model Konsumen */

$this->breadcrumbs=array(
	'Konsumens'=>array('index'),
	$model->id_konsumen,
);

$this->menu=array(
	array('label'=>'Daftar Konsumen', 'url'=>array('index')),
	array('label'=>'Tambah Konsumen', 'url'=>array('create')),
	array('label'=>'Update Konsumen', 'url'=>array('update', 'id'=>$model->id_konsumen)),
	array('label'=>'Hapus Konsumen', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_konsumen),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Atur Konsumen', 'url'=>array('admin')),
);
?>

<h1>Lihat Konsumen Atas Nama <?php echo $model->nama; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_konsumen',
		'nama',
		'no_telp',
		'alamat',
	),
)); ?>
