<?php
/* @var $this KonsumenController */
/* @var $model Konsumen */

$this->breadcrumbs=array(
	'Konsumens'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Konsumen', 'url'=>array('index')),
	array('label'=>'Atur Konsumen', 'url'=>array('admin')),
);
?>

<h1>Tambah Konsumen</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>