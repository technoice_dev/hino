<?php
/* @var $this KonsumenController */
/* @var $model Konsumen */

$this->breadcrumbs=array(
	'Konsumens'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Konsumen', 'url'=>array('index')),
	array('label'=>'Create Konsumen', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#konsumen-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Tabel Konsumen</h1>


<?php echo CHtml::link('Pencarian','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'konsumen-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_konsumen',
		'nama',
		'no_telp',
		'alamat',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
