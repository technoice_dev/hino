<?php
/* @var $this KonsumenController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Konsumen',
);

$this->menu=array(
	array('label'=>'Tambah Konsumen', 'url'=>array('create')),
	array('label'=>'Atur Konsumen', 'url'=>array('admin')),
);
?>

<h1>Konsumen</h1>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="container">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/konsumen/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/konsumen/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>



<div class="row">
    <div class="container">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <td style="text-align:center;">ID Konsumen</td>
                    <td style="text-align:center;">Nama Konsumen</td>
                    <td style="text-align:center;">No Telp</td>
                    <td style="text-align:left;">Alamat</td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= $row->id_konsumen?></td>
                        <td><?= $row->nama?></td>
                        <td style="text-align:center;"><?= $row->no_telp?></td>
                        <td style="text-align:left;"><?= $row->alamat?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/konsumen/update/id/<?= $row->nik?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/konsumen/delete/id/<?= $row->nik?>" class="btn btn-danger btn-sm">Hapus</a> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/konsumen/view/id/<?= $row->nik?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>

