<?php
/* @var $this UnitmasterController */
/* @var $model Unitmaster */

$this->breadcrumbs=array(
	'Unitmasters'=>array('index'),
	$model->unit_id,
);

$this->menu=array(
	array('label'=>'List Unitmaster', 'url'=>array('index')),
	array('label'=>'Create Unitmaster', 'url'=>array('create')),
	array('label'=>'Update Unitmaster', 'url'=>array('update', 'id'=>$model->unit_id)),
	array('label'=>'Delete Unitmaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->unit_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Unitmaster', 'url'=>array('admin')),
);
?>

<h1>View Unit Master</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/unitmaster" class="btn btn-primary">Unit List</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Unit ID</td>
                <td><?= $model->unit_id ?></td>
            </tr>
            <tr>
                <td>Unit Type</td>
                <td><?= $model->unit_type ?></td>
            </tr>
            <tr>
                <td>Unit Name</td>
                <td><?= $model->unit_name ?></td>
            </tr>
        </table>
    </div>
</div>
