<?php
/* @var $this UnitmasterController */
/* @var $model Unitmaster */

$this->breadcrumbs=array(
	'Unitmasters'=>array('index'),
	'Create',
);

?>

<h1>Add Unit</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>