<?php
/* @var $this UnitmasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Unit Master',
);

$this->menu=array(
	array('label'=>'Create Unitmaster', 'url'=>array('create')),
	array('label'=>'Manage Unitmaster', 'url'=>array('admin')),
);
?>
<div class="row">
    <h1>Unit Master</h1>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/unitmaster/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/unitmaster/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>

<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus Unit Master ini ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <?php
                        $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/masterDealer?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/customer?';
                        $sortByUnitType = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'cust_name' ?  $_GET['sortBy'] : 'cust_name';
                        $sortByUnitName = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'phone' ?  $_GET['sortBy'] : 'phone';
                        $sortByQty = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'phone' ?  $_GET['sortBy'] : 'phone';
                        $sortByKmInterval = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'phone' ?  $_GET['sortBy'] : 'phone';
                        $sortByProbabilitas = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'phone' ?  $_GET['sortBy'] : 'phone';
                        $sortByFrekGanti = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'phone' ?  $_GET['sortBy'] : 'phone';
                        $sortByMileage = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'phone' ?  $_GET['sortBy'] : 'phone';
                        $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                    ?>
                    <td style="text-align:center; width:100px">Unit ID</td>
                    <td style="text-align:center;">Unit Type</td>
                    <td style="text-align:center;">Unit Name</td>
                    <td style="text-align:center; width:200px;">Action</td>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach($model as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td style="text-align:center;"><?= $row->unit_type?></td>
                        <td style="text-align:center;"><?= $row->unit_name?></td>
                        <td style="text-align:center; "><a href="<?= Yii::app()->baseUrl ;?>/Cpk/unitmaster/update/id/<?= $row->unit_id?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <?php if(count($row->partbytype) > 0 || count($row->invDet) > 0 ) { 

                            } else { ?>
                            
                            <button data-val="<?= $row->unit_id?>" class="btn btn-danger btn-sm del-button">Hapus</button> 
                            <?php } ?>
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/unitmaster/view/id/<?= $row->unit_id?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>
<script>
    $('#date-from').datepicker({
        format : 'yyyy-mm-dd',
    });
    $('#date-to').datepicker({
        format : 'yyyy-mm-dd',
    });
    
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/unitmaster/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>
