<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->nik,
);
?>
<h1>View Mechanic</h1>
<a href="<?= Yii::app()->baseUrl ;?>/Cpk/mechanic" class="btn btn-primary">Mechanic List</a>
<br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="alert alert-<?=$key; ?>" role="alert">
  <?=$message; ?>
</div>
<?php } ?>
<br>
<div class="card">
    <div class="card-body">
        <table class="table">
            <tr>
                <td>NIK</td>
                <td><?= $model->nik ?></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><?= $model->nama ?></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td><?= $model->alamat ?></td>
            </tr>
            <tr>
                <td>No Telp</td>
                <td><?= $model->no_telp ?></td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td><?= $model->jabatan ?></td>
            </tr>
        </table>
    </div>
</div>
