<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs = array(
	'Mechanics' => array('index'),
	'Create',
);

$this->menu = array(
	array('label' => 'List Pegawai', 'url' => array('index')),
	array('label' => 'Atur Pegawai', 'url' => array('admin')),
);
?>

<h1>Tambah Pegawai</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>