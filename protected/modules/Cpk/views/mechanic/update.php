<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->nik=>array('view','id'=>$model->nik),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pegawai', 'url'=>array('index')),
	array('label'=>'Tambah Pegawai', 'url'=>array('create')),
	array('label'=>'Lihat Pegawai', 'url'=>array('view', 'id'=>$model->nik)),
	array('label'=>'Atur Pegawao', 'url'=>array('admin')),
);
?>

<h1>Update Pegawai <?php echo $model->nik; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>