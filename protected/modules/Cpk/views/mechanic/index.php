<?php
/* @var $this MechanicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pegawai',
);

$this->menu=array(
	array('label'=>'Tambah Pegawai', 'url'=>array('create')),
	array('label'=>'Atur Pegawai', 'url'=>array('admin')),
);
?>
<div class="row">
    <h1>Pegawai</h1>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/mechanic/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/mechanic/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>



<div class="row">
    <div class="">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                    <td style="text-align:center;">NIK</td>
                    <td style="text-align:center;">Nama Mekanik</td>
                    <td style="text-align:center;">No Telp</td>
                    <td style="text-align:left;">Jabatan</td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= $row->nik?></td>
                        <td><?= $row->nama?></td>
                        <td style="text-align:center;"><?= $row->no_telp?></td>
                        <td style="text-align:left;"><?= $row->jabatan?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/mechanic/update/id/<?= $row->nik?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/mechanic/delete/id/<?= $row->nik?>" class="btn btn-danger btn-sm">Hapus</a> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/mechanic/view/id/<?= $row->nik?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>
