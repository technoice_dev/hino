<?php
/* @var $this InvPowerController */
/* @var $model InvPower */

$this->breadcrumbs=array(
	'Inv Powers'=>array('index'),
	$model->inv_id=>array('view','id'=>$model->inv_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvPower', 'url'=>array('index')),
	array('label'=>'Create InvPower', 'url'=>array('create')),
	array('label'=>'View InvPower', 'url'=>array('view', 'id'=>$model->inv_id)),
	array('label'=>'Manage InvPower', 'url'=>array('admin')),
);
?>

<h1>Update InvPower <?php echo $model->inv_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>