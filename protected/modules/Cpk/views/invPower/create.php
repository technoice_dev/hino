<?php
/* @var $this InvPowerController */
/* @var $model InvPower */

$this->breadcrumbs=array(
	'Inv Powers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InvPower', 'url'=>array('index')),
	array('label'=>'Manage InvPower', 'url'=>array('admin')),
);
?>

<h1>Create InvPower</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>