<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manpower',
);


?>

<div class="row">
    <h2>Manpower List</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/manpower/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/manpower/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>


<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="table-responsive" style="overflow-x:auto;">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <?php
    
                    $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/manpower?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/manpower?';
                    $sortByBscSalary = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'bsc_sallary' ?  $_GET['sortBy'] : 'bsc_sallary';
                    $sortByPph = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'pph' ?  $_GET['sortBy'] : 'pph';
                    $sortByLaundry = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'laundry' ?  $_GET['sortBy'] : 'laundry';
                    $sortByTransport = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'transport' ?  $_GET['sortBy'] : 'transport';
                    $sortByMeal = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'meal' ?  $_GET['sortBy'] : 'meal';
                    $sortByMedical = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'medical' ?  $_GET['sortBy'] : 'medical';
                    $sortByMobilePhone = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'mobile_phone' ?  $_GET['sortBy'] : 'mobile_phone';
                    $sortByBPJS = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'bpjs' ?  $_GET['sortBy'] : 'bpjs';
                    $sortByTHR = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'thr' ?  $_GET['sortBy'] : 'thr';
                    $sortByUniSafety = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'uni_safety' ?  $_GET['sortBy'] : 'uni_safety';
                    $sortByHardship = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'harship' ?  $_GET['sortBy'] : 'harship';
                    $sortByOvertime = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'overtime' ?  $_GET['sortBy'] : 'overtime';
                    $sortByOtherPay = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'other_pay' ?  $_GET['sortBy'] : 'other_pay';
                    $sortByCategory = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'category' ?  $_GET['sortBy'] : 'categoy';
                    $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                ?>
                <tr>
                    <td style="text-align:center;">No</td>
                    <td><a href="<?= $url.'sortBy='.$sortByBscSalary.'&sortType='.$sortType?>">Bsc Salary</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByPph.'&sortType='.$sortType?>">Pph</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByLaundry.'&sortType='.$sortType?>">Laundry</a></td>
                    <td style="text-align:right;"><a href="<?= $url.'sortBy='.$sortByTransport.'&sortType='.$sortType?>">Transport</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByMeal.'&sortType='.$sortType?>">Meal</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByMedical.'&sortType='.$sortType?>">Medical</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByMobilePhone.'&sortType='.$sortType?>">Mobile Phone</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByBPJS.'&sortType='.$sortType?>">BPJS</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByTHR.'&sortType='.$sortType?>">THR</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByUniSafety.'&sortType='.$sortType?>">Uniform + Safety</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByHardship.'&sortType='.$sortType?>">Hardship</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByOvertime.'&sortType='.$sortType?>">Overtime</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByOtherPay.'&sortType='.$sortType?>">Other Pay</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByCategory.'&sortType='.$sortType?>">Category</a></td>
                    <td style="text-align:center;">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= number_format($row->bsc_sallary)?></td>
                        <td><?= number_format($row->pph)?></td>
                        <td style="text-align:right;"><?= number_format($row->laundry)?></td>
                        <td style="text-align:right;"><?= number_format($row->transport)?></td>
                        <td><?= number_format($row->meal)?></td>
                        <td><?= number_format($row->medical)?></td>
                        <td><?= number_format($row->mobile_phone)?></td>
                        <td><?= number_format($row->bpjs)?></td>
                        <td><?= number_format($row->thr)?></td>
                        <td><?= number_format($row->uni_safety)?></td>
                        <td><?= number_format($row->hardship)?></td>
                        <td><?= number_format($row->overtime)?></td>
                        <td><?= number_format($row->other_pay)?></td>
                        <td><?= $row->kategory?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/manpower/update/id/<?= $row->mp_id?>" class="btn btn-primary btn-xs">Ubah</a> 
                            <button data-val="<?= $row->mp_id?>" class="btn btn-danger btn-sm del-button">Hapus</button> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/manpower/view/id/<?= $row->mp_id?>" class="btn btn-info btn-xs">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>

<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/manpower/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>

