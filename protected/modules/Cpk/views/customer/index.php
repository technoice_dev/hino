<?php
/* @var $this MasterPartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Customer',
);


?>

<div class="row">
    <h2>Customer</h2>
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {   
?>
<div class="row">
    <div class="alert alert-<?=$key; ?>" role="alert">
        <?=$message; ?>
    </div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="">
        <div class="col pull-left">
            <a class="btn btn-primary" href="<?= Yii::app()->baseUrl ;?>/Cpk/customer/create" align="right">Tambah</a>
        </div>
        <div class="col pull-right">
            <form class="form-inline" action="<?= Yii::app()->baseUrl ;?>/Cpk/customer/index" method="get">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="search" placeholder="Cari" value="<?= $search ?>">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
        </div>
        <br>
    </div>
    
</div>
<br>


<div class="modal" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h4 class="modal-title">Delete Confirmation</h4></center>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="deleted-id">
                <center>
                    <p>Anda yakin ingin menghapus data ?</p>
                    <p>
                        <button id="delete-data" class="btn btn-danger btn-sm">Ya</button>
                        <button id="cancel-btn" data-dismiss="modal" class="btn btn-warning btn-sm">Tidak</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive">
        <table class="table table-stripped table-hover table-bordered">
            <thead>
                <tr>
                <?php
    
                        $url = isset($_GET['search'])? Yii::app()->baseUrl.'/Cpk/customer?search='.$_GET['search'].'&' : Yii::app()->baseUrl.'/Cpk/customer?';
                        $sortByCustName = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'cust_name' ?  $_GET['sortBy'] : 'cust_name';
                        $sortByPhone = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'phone' ?  $_GET['sortBy'] : 'phone';
                        $sortByAddress = (isset($_GET['sortBy']) ? $_GET['sortBy'] : ($_GET['sortBy'] = '')) == 'addres' ?  $_GET['sortBy'] : 'addres';
                        $sortType = isset($_GET['sortType']) ? ($_GET['sortType'] == 'asc'? 'desc': 'asc') : 'asc';
                ?>
                    <td style="text-align:center;">No</td>
                    <td><a href=""><a href="<?= $url.'sortBy='.$sortByCustName.'&sortType='.$sortType?>">Customer Name</a></td>
                    <td><a href="<?= $url.'sortBy='.$sortByPhone.'&sortType='.$sortType?>">Phone</td>
                    <td><a href="<?= $url.'sortBy='.$sortByAddress.'&sortType='.$sortType?>">Address</td>
                    <td style="text-align:center;">Action</td>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $key => $row) {?>
                    <tr>
                        <td style="text-align:center;"><?= ($pages->currentPage * $pages->pageSize)  + $key + 1  ?></td>
                        <td><?= $row->cust_name?></td>
                        <td><?= $row->phone?></td>
                        <td><?= $row->addres?></td>
                        <td style="text-align:center;"><a href="<?= Yii::app()->baseUrl ;?>/Cpk/customer/update/id/<?= $row->id_cust?>" class="btn btn-primary btn-sm">Ubah</a> 
                            <button data-val="<?= $row->id_cust?>" class="btn btn-danger btn-sm del-button">Hapus</button> 
                            <a href="<?= Yii::app()->baseUrl ;?>/Cpk/customer/view/id/<?= $row->id_cust?>" class="btn btn-info btn-sm">View</a></td>
                    </tr>
            
                <?php } ?>
            </tbody>
        </table>
        <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
    </div>
</div>
<script>
$('table tbody tr td .del-button').click(function(){
    var data_id = $(this).attr('data-val');
    $('#deleted-id').val(data_id);
    $('#delete-modal').modal('show');
});
$('#delete-data').click(function(){
    var delete_data = $('#deleted-id').val();
    var delete_url = '<?= Yii::app()->baseUrl ;?>/Cpk/customer/delete/id/'+delete_data;
    window.location.href = delete_url;
});

</script>


