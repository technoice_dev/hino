<?php
/* @var $this InvSafetyController */
/* @var $model InvSafety */

$this->breadcrumbs=array(
	'Inv Safeties'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InvSafety', 'url'=>array('index')),
	array('label'=>'Manage InvSafety', 'url'=>array('admin')),
);
?>

<h1>Create InvSafety</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>