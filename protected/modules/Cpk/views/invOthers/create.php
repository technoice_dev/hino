<?php
/* @var $this InvOthersController */
/* @var $model InvOthers */

$this->breadcrumbs=array(
	'Inv Others'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InvOthers', 'url'=>array('index')),
	array('label'=>'Manage InvOthers', 'url'=>array('admin')),
);
?>

<h1>Create InvOthers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>