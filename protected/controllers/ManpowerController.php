<?php

class ManpowerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Manpower;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manpower']))
		{
			$bsc_sallary = str_replace(',','',$_POST['Manpower']['bsc_sallary']);
			$pph = str_replace(',','',$_POST['Manpower']['pph']);
			$laundry = str_replace(',','',$_POST['Manpower']['laundry']);
			$transport = str_replace(',','',$_POST['Manpower']['transport']);
			$meal = str_replace(',','',$_POST['Manpower']['meal']);
			$medical = str_replace(',','',$_POST['Manpower']['medical']);
			$mobile_phone = str_replace(',','',$_POST['Manpower']['mobile_phone']);
			$bpjs = str_replace(',','',$_POST['Manpower']['bpjs']);
			$thr = str_replace(',','',$_POST['Manpower']['thr']);
			$uni_safety = str_replace(',','',$_POST['Manpower']['uni_safety']);
			$hardship = str_replace(',','',$_POST['Manpower']['hardship']);
			$overtime = str_replace(',','',$_POST['Manpower']['overtime']);
			$other_pay = str_replace(',','',$_POST['Manpower']['other_pay']);
			
			$model->attributes=$_POST['Manpower'];
			$model->bsc_sallary = $bsc_sallary;
			$model->pph = $pph;
			$model->laundry = $laundry;
			$model->transport = $transport;
			$model->meal = $meal;
			$model->medical = $medical;
			$model->mobile_phone = $mobile_phone;
			$model->bpjs = $bpjs;
			$model->thr = $thr;
			$model->uni_safety = $uni_safety;
			$model->hardship = $hardship;
			$model->overtime = $overtime;
			$model->other_pay = $other_pay;
			if($model->save())
				$this->redirect(array('view','id'=>$model->mp_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manpower']))
		{
			$model->attributes=$_POST['Manpower'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->mp_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Manpower');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Manpower('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Manpower']))
			$model->attributes=$_GET['Manpower'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Manpower the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Manpower::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Manpower $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manpower-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
